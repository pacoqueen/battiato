#!/usr/bin/env python

"""
TO-DO: ¿Y las tags? Ahora que uso TODO y DONE debería poder filtrar y agrupar
también por tags para saber más o menos el número de incidencias/tareas
tratadas, resueltas y pendientes (OJO: algunas TODO se convierten después en
DONE, no se quedan pendientes para siempre).
"""

import os

import streamlit as st
import numpy as np
import pandas as pd
import altair as alt

import sqlite3
import datetime, calendar

from collections import defaultdict

DAYS = ((0, "Lunes"),
        (1, "Martes"),
        (2, "Miércoles"),
        (3, "Jueves"),
        (4, "Viernes"),
        (5, "Sábado"),
        (6, "Domingo"))

MONTHS = tuple([i for i in enumerate(('Enero',   'Febrero',   'Marzo',
                                      'Abril',   'Mayo',      'Junio',
                                      'Julio',   'Agosto',    'Septiembre',
                                      'Octubre', 'Noviembre', 'Diciembre'), 1)])

HAMSTERDBPATH = os.path.join(os.environ['HOME'], 'Nextcloud', 'hamster-applet',
                             'hamster.db')

def open_db(pathdb):
    """Open database file and returns connection object."""
    con = sqlite3.connect(pathdb)
    return con

def close_db(con):
    """Closes database connection."""
    con.close()

def load_categories(con):
    """Load categories from database and returns [(id, categorie)...] list."""
    cur = con.cursor()
    categorias = []
    for row in cur.execute('SELECT id, name FROM categories ORDER BY id'):
        categorias.append(row)
    return categorias

def build_sidebar(con):
    """Initialize sidebar with a multiple selectbox with all categories."""
    pathdb = st.sidebar.text_input(label="BD hamster (sqlite)",
            value = HAMSTERDBPATH, help="Actualmente no es configurable.")
    categorias = load_categories(con)
    nombres_categorias = tuple([cat[1] for cat in categorias])
    selectbox = st.sidebar.multiselect('Categoría', nombres_categorias,
                                       default=nombres_categorias)
    return categorias, selectbox, pathdb

def load_facts(con, ids_categoria=[]):
    """Read and return all facts from database in a list."""
    sqlquery = """SELECT start_time, end_time, categories.id AS id_categoria
        FROM facts
        LEFT JOIN activities ON facts.activity_id = activities.id
        LEFT JOIN categories ON activities.category_id = categories.id"""
    cur = con.cursor()
    if ids_categoria:
        sqlquery += " WHERE"
        sqlquery += " OR".join([" id_categoria = ?"]*len(ids_categoria))
    sqlquery += " ORDER BY start_time"
    if ids_categoria:
        facts = cur.execute(sqlquery, ids_categoria)
    else:
        facts = cur.execute(sqlquery)
    return facts.fetchall()

def calcular_duracion_horas(fact):
    """Returns time elapsed from a fact."""
    startdate, enddate, idcategoria = fact
    fechahoraini = datetime.datetime.strptime(startdate, "%Y-%m-%d %H:%M:%S")
    try:
        fechahorafin = datetime.datetime.strptime(enddate, "%Y-%m-%d %H:%M:%S")
    except TypeError:
        # No tiene hora de finalización. Tarea en curso.
        fechahorafin = datetime.datetime.now()
    duracion = fechahorafin - fechahoraini
    # duracion_horas = round(duracion.total_seconds()/60/60)
    duracion_horas = duracion.total_seconds()/60/60
    return duracion_horas

def parse_facts_por_dia_y_categoria(facts, categorias,
                                    first_date=None, last_date=None):
    """
    Build a numpy array with total elapsed time for every day of week
    and categorie.
    """
    arrayhoras = np.zeros((len(categorias), 7))
    for fact in facts:
        startdate, enddate, idcategoria = fact
        fechahoraini = datetime.datetime.strptime(startdate, "%Y-%m-%d %H:%M:%S")
        try:
            fechahorafin = datetime.datetime.strptime(enddate, "%Y-%m-%d %H:%M:%S")
        except TypeError:
            # No tiene hora de finalización. Tarea en curso.
            fechahorafin = datetime.datetime.now()
        if first_date and fechahoraini < first_date:
            continue
        if last_date and fechahoraini >= last_date:
            continue
        duracion_horas = calcular_duracion_horas(fact)
        index_idcategoria = [c[0] for c in categorias if c[0] > 0].index(idcategoria)
        diasemana = fechahoraini.weekday()
        arrayhoras[index_idcategoria][diasemana] += duracion_horas
    return arrayhoras

def parse_facts_por_mes_y_categoria(facts, categorias,
                                    first_date=None, last_date=None):
    """
    Build a numpy array with total elapsed time for every month
    and categorie.
    """
    arrayhoras = np.zeros((len(categorias), len(MONTHS)))
    for fact in facts:
        startdate, enddate, idcategoria = fact
        fechahoraini = datetime.datetime.strptime(startdate, "%Y-%m-%d %H:%M:%S")
        try:
            fechahorafin = datetime.datetime.strptime(enddate, "%Y-%m-%d %H:%M:%S")
        except TypeError:
            # No tiene hora de finalización. Tarea en curso.
            fechahorafin = datetime.datetime.now()
        if first_date and fechahoraini < first_date:
            continue
        if last_date and fechahoraini >= last_date:
            continue
        duracion_horas = calcular_duracion_horas(fact)
        index_idcategoria = [c[0] for c in categorias if c[0] > 0].index(idcategoria)
        mes = fechahoraini.month - 1
        arrayhoras[index_idcategoria][mes] += duracion_horas
    horas = []
    for idmes, mes in MONTHS:
        for idcategoria, categoria in categorias:
            # print(idcategoria, categoria, idmes, mes)
            index_idcategoria = [c[0] for c in categorias if c[0] > 0].index(idcategoria)
            # print(index_idcategoria)
            # horas.append({categoria: arrayhoras[index_idcategoria][idmes-1]})
            horas.append(arrayhoras[index_idcategoria][idmes-1])
    meses = []
    for m in MONTHS:
        for i in range(len(categorias)):
            meses.append(("{:02}".format(m[0]), m[1]))
    res = {'categoría': [("{:02}".format(m[0]), m[1]) for m in categorias] * len(MONTHS),
           'horas': horas,
           'mes': meses}
    return res

def parse_facts_por_mes_y_categoria_resume(facts, categorias,
                                           first_date=None, last_date=None):
    """
    Build a numpy array with total elapsed time for every month
    and category.
    """
    arrayhoras = np.zeros((len(categorias), len(MONTHS)))
    for fact in facts:
        startdate, enddate, idcategoria = fact
        fechahoraini = datetime.datetime.strptime(startdate, "%Y-%m-%d %H:%M:%S")
        try:
            fechahorafin = datetime.datetime.strptime(enddate, "%Y-%m-%d %H:%M:%S")
        except TypeError:
            # No tiene hora de finalización. Tarea en curso.
            fechahorafin = datetime.datetime.now()
        if first_date and fechahoraini < first_date:
            continue
        if last_date and fechahoraini >= last_date:
            continue
        duracion_horas = calcular_duracion_horas(fact)
        index_idcategoria = [c[0] for c in categorias if c[0] > 0].index(idcategoria)
        mes = fechahoraini.month - 1
        arrayhoras[index_idcategoria][mes] += duracion_horas
    return arrayhoras


def plot_horas_por_mes(con, facts, categorias):
    """
    It draws a bar chart and a dataframe with elapsed times by month and
    categorie.
    """
    first_year = datetime.datetime.strptime(facts[0][0], "%Y-%m-%d %H:%M:%S").year
    last_year = datetime.datetime.strptime(facts[-1][0], "%Y-%m-%d %H:%M:%S").year
    if first_year == last_year:
        first_year -= 1
    selectyear = st.sidebar.slider("Año", first_year, last_year, (first_year, last_year))
    selectmonthini = st.sidebar.slider("Mes año inicio", 1, 12, value=1)
    last_month = calendar.monthrange(first_year, selectmonthini)[1]
    selectdayini = st.sidebar.slider("Día año inicio", 1, last_month, value=1)
    selectmonthfin = st.sidebar.slider("Mes año fin", 1, 12, value=12)
    last_month = calendar.monthrange(last_year, selectmonthfin)[1]
    selectdayfin = st.sidebar.slider("Día año fin", 1, last_month, value=last_month)
    first_date = datetime.datetime(selectyear[0], selectmonthini, selectdayini)
    last_date = datetime.datetime(selectyear[1], selectmonthfin, selectdayfin)
    last_date += datetime.timedelta(1)
    facts_por_mes = parse_facts_por_mes_y_categoria(facts, categorias,
                                                    first_date, last_date)
    # print(facts_por_mes)
    # print(len(facts_por_mes['mes']), len(facts_por_mes['categoría']), len(facts_por_mes['horas']))
    dataframe = pd.DataFrame(facts_por_mes)
    # st.write(dataframe)
    # print(dataframe)
    bar_chart = alt.Chart(dataframe).mark_bar().encode(
            x = "mes:O",
            y = "sum(horas):Q",
            color = "categoría:N")
    st.altair_chart(bar_chart, use_container_width=True)

    columnas = [month[1] for month in MONTHS]
    resume_dataframe = pd.DataFrame(
            parse_facts_por_mes_y_categoria_resume(facts, categorias,
                                                   first_date, last_date),
            index=[c[1] for c in categorias if c[0] > 0],
            columns=columnas)
    resume_dataframe.loc['Total', :] = resume_dataframe.sum(axis=0)
    resume_dataframe.loc[:, 'Total'] = resume_dataframe.sum(axis=1)
    st.dataframe(resume_dataframe.round().astype('int32'
        ).style.highlight_max(axis=0).highlight_max(axis=1))
    return selectyear, selectmonthini, selectdayini, selectmonthfin, selectdayfin

def plot_horas_por_dia(con, facts, categorias,
                       selectyear, selectmonthini, selectdayini,
                       selectmonthfin, selectdayfin):
    """
    It draws a bar chart and a dataframe with elapsed times by day and
    categorie.
    """
    first_year = datetime.datetime.strptime(facts[0][0], "%Y-%m-%d %H:%M:%S").year
    last_year = datetime.datetime.strptime(facts[-1][0], "%Y-%m-%d %H:%M:%S").year
    if first_year == last_year:
        first_year -= 1
    # selectyear = st.sidebar.slider("Año", first_year, last_year, (first_year, last_year))
    # selectmonthini = st.sidebar.slider("Mes año inicio", 1, 12, value=1)
    last_day = calendar.monthrange(first_year, selectmonthini)[1]
    # selectdayini = st.sidebar.slider("Día año inicio", 1, last_day, value=1)
    # selectmonthfin = st.sidebar.slider("Mes año fin", 1, 12, value=12)
    last_day = calendar.monthrange(last_year, selectmonthfin)[1]
    # selectdayfin = st.sidebar.slider("Día año fin", 1, last_day, value=last_day)
    first_date = datetime.datetime(selectyear[0], selectmonthini, selectdayini)
    last_date = datetime.datetime(selectyear[1], selectmonthfin, selectdayfin)
    last_date += datetime.timedelta(1)
    facts_por_dia = parse_facts_por_dia_y_categoria(facts, categorias,
                                                    first_date, last_date)
    columnas = [weekday[1] for weekday in DAYS]
    dataframe = pd.DataFrame(
            facts_por_dia,
            index=[c[1] for c in categorias if c[0] > 0],
            columns=columnas)
    # st.write(dataframe)
    _columnas = dataframe.columns
    columnas = ["[{}] {}".format(weekday[0], weekday[1]) for weekday in DAYS]
    dataframe.columns = columnas
    st.bar_chart(dataframe.transpose())
    dataframe.columns = _columnas
    dataframe.loc['Total', :] = dataframe.sum(axis=0)
    dataframe.loc[:, 'Total'] = dataframe.sum(axis=1)
    st.dataframe(dataframe.round().astype('int32'
        ).style.highlight_max(axis=0).highlight_max(axis=1))
    # st.write(facts)

def plot_horas_por_anno(con, facts):
    """It draws a couple of charts with elapsed time by month and year."""
    data = {}
    for fact in facts:
        anno = str(datetime.datetime.strptime(fact[0], "%Y-%m-%d %H:%M:%S").year)
        mes = "{:02d}".format(datetime.datetime.strptime(fact[0], "%Y-%m-%d %H:%M:%S").month)
        if anno not in data:
            data[anno] = defaultdict(lambda: 0)
        data[anno][mes] += calcular_duracion_horas(fact)
    dataframe = pd.DataFrame(data,
            index = ["{:02d}".format(mes) for mes in range(1, 13)])
    dataframe = dataframe.fillna(0)
    datagraph = dataframe.T
    left_col, right_col = st.columns(2)
    left_col.bar_chart(datagraph)
    right_col.area_chart(datagraph)
    dataframe.loc['Total', :] = dataframe.sum(axis=0)
    st.write(dataframe.round().astype('int32'))

def main():
    """
    Rutina principal.
    """
    pathdb = HAMSTERDBPATH
    con = open_db(pathdb)
    categorias, selectbox_categorias, pathdb = build_sidebar(con)
    try:
        ids_categoria = [c[0] for c in categorias if c[1] in selectbox_categorias]
    except IndexError:
        ids_categoria = []
    # st.write(ids_categoria)
    facts = load_facts(con, ids_categoria)  # Devuelve ya filtrados por categoría
    selectyear, selectmonthini, selectdayini, selectmonthfin, selecthayfin \
            = plot_horas_por_mes(con, facts, categorias)
    plot_horas_por_dia(con, facts, categorias,
                       selectyear, selectmonthini, selectdayini,
                       selectmonthfin, selecthayfin)
    plot_horas_por_anno(con, facts)
    close_db(con)

if __name__ == "__main__":
    main()


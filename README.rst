Battiato
========

> Franco Battiato nos ha dejado el 18 de mayo de 2021.
> Se nos fue un genio. *Il tempo stesso*.

Extrae estadísticas del *SQLite* de *Hamster* para ver:
- Horas trabajadas de media por día de la semana.
- Total de horas trabajadas por semana, mes y año entre rangos de fechas.


Screenshots
-----------
.. image:: battiato.png
    :target: battiato.png

.. image:: battiato.webm
    :target:: battiato.webm


Dependencies
------------
`poetry <https://python-poetry.org/docs/pyproject/>`_

`streamlit <https://docs.streamlit.io/en/stable/main_concepts.html>`_

Pandas

Numpy

sqlite3


Run
---

poetry run streamlit run battiato/main.py
